#!/bin/sh
export CATALINA_BASE="<%= home %>"
<%= startup_snippet %>
/usr/share/tomcat7/bin/startup.sh
echo "Tomcat started"
