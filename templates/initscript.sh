#!/bin/bash

TOMCAT_USER=<%= user %>
TOMCAT_APP_HOME=<%= home %>
STARTUP_SH="<%= home %>/bin/startup.sh"
SHUTDOWN_SH="<%= home %>/bin/shutdown.sh"

me=$(/usr/bin/whoami)
service="tomcat7-<%= user %>"

case "${me}" in
  root)
    sudo="sudo -u ${TOMCAT_USER}"
    ;;
  ${TOMCAT_USER})
    sudo=''
    ;;
  *)
    echo "Launch this script as ${TOMCAT_USER} or root."
    exit
    ;;
esac

case "$1" in
  start)
    ${sudo} ${STARTUP_SH}
    exit $?
    ;;
  stop)
    ${sudo} ${SHUTDOWN_SH}
    exit $?
    ;;
  restart)
    ${sudo} ${SHUTDOWN_SH}
    ${sudo} ${STARTUP_SH}
    exit $?
    ;;
  status)
    if [ $(/usr/bin/pgrep -c -u ${TOMCAT_USER} java) -eq 0 ]; then
      echo "${service} is not running."
      exit 1
    else
      echo "${service} is running."
      exit 0
    fi
    ;;
  *)
    echo "Usage: %0 <start|stop|restart|status>"
    exit 1
    ;;
esac

