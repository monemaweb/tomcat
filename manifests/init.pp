class tomcat ($instance_root = '/srv/tomcat') {

  if !defined(Package['openjdk-6-jre-headless']) {
    package { 'openjdk-6-jre-headless':
      ensure => present
    }
  }
  if !defined(Package['paxctl']) {
    package { 'paxctl':
      ensure => present
    }
  }

  exec { 'allow-java-on-grsec':
    command     => '/sbin/paxctl -Cxsperm /usr/bin/java',
    require     => Package['paxctl'],
    subscribe   => Package['openjdk-6-jre-headless'],
    refreshonly => true
  }

  file { $instance_root:
    ensure  => directory
  }

  package { [ 'tomcat7',
    'libtcnative-1',
    'tomcat7-admin']:
    ensure  => present,
    require => Exec['allow-java-on-grsec']
  }
}
