
define tomcat::instance($manage_pw, $deploy_pw, $http_port, $control_port, $startup_snippet = '', $enabled = true, $global_naming_snippet = '', $ssh_key_type = false, $ssh_key = false) {

  # Make sure we have a working tomcat setup
  #Class['tomcat'] -> Class['tomcat::instance']
  Class['tomcat'] -> Tomcat::Instance[$name]

  # Comfort variables
  $user = $name
  $home = "${tomcat::instance_root}/${user}"
  $dirs = [ "${home}/bin", "${home}/conf", "${home}/logs", "${home}/lib",
    "${home}/temp", "${home}/work", "${home}/shared" ]
  $conf_files = ["${home}/conf/catalina.properties", "${home}/conf/context.xml",
        "${home}/conf/logging.properties", "${home}/conf/server.xml",
        "${home}/conf/web.xml", "${home}/conf/tomcat-users.xml" ]

  # Resource defaults
  File {
    ensure  => present,
    owner   => $user,
    group   => $user,
  }

  # Unix user related:
  # User
  user { $user:
    ensure  => present,
    home    => $home,
    shell   => '/bin/bash'
  }
  # Group
  group { $user:
    ensure  => present,
  }
  # Home
  file { $home:
    ensure  => directory,
    mode    => '0701',
    require => [ User[$user], Group[$user] ]
  }

  if $ssh_key {
    if $ssh_key_type {
      ssh_authorized_key { $user:
        ensure  => present,
        user    => $user,
        type    => $ssh_key_type,
        key     => $ssh_key,
      }
    }
  }

  # Basic tomcat structure
  file { $dirs:
    ensure  => directory,
    mode    => '0700',
    require => File[$home]
  }
  file { "${home}/webapps":
    ensure  => directory,
    mode    => '0701',
    require => File[$home]
  }

  # Startup / shutdown scripts
  file { "${home}/bin/setenv.sh":
    mode    => '0755',
    content => template('tomcat/setenv.sh'),
    require => File[$dirs]
  }
  file { "${home}/bin/startup.sh":
    mode    => '0775',
    content => template('tomcat/startup.sh'),
    require => File[$dirs]
  }
  file { "${home}/bin/shutdown.sh":
    mode    => '0775',
    content => template('tomcat/shutdown.sh'),
    require => File[$dirs]
  }


  # Configuration
  file { "${home}/conf/catalina.properties":
    mode    => '0644',
    content => template('tomcat/catalina.properties'),
    require => File[$dirs]
  }
  file { "${home}/conf/context.xml":
    mode    => '0644',
    content => template('tomcat/context.xml'),
    require => File[$dirs]
  }
  file { "${home}/conf/logging.properties":
    mode    => '0644',
    content => template('tomcat/logging.properties'),
    require => File[$dirs]
  }
  file { "${home}/conf/server.xml":
    mode    => '0644',
    content => template('tomcat/server.xml'),
    require => File[$dirs]
  }
  file { "${home}/conf/web.xml":
    mode    => '0644',
    content => template('tomcat/web.xml'),
    require => File[$dirs]
  }
  file { "${home}/conf/tomcat-users.xml":
    mode    => '0644',
    content => template('tomcat/tomcat-users.xml'),
    require => File[$dirs]
  }

  # Manager
  file { "${home}/webapps/manager":
    ensure  => link,
    target  => '/usr/share/tomcat7-admin/manager',
    require => File["${home}/webapps"]
  }

  # Init script
  file { "/etc/init.d/tomcat7-${user}":
    ensure  => present,
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    content => template('tomcat/initscript.sh'),
    require => File[$conf_files]
  }

  if $enabled == true {
    service { "tomcat7-${user}":
      ensure  => running,
      enable  => true,
      require => File["/etc/init.d/tomcat7-${user}"]
    }
  } else {
    service { "tomcat7-${user}":
      ensure  => stopped,
      enable  => false,
      require => File["/etc/init.d/tomcat7-${user}"]
    }
  }
}
