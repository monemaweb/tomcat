define tomcat::lib($instance, $source) {
  file { "${tomcat::instance_root}/${instance}/lib/${name}":
    ensure  => present,
    owner   => $instance,
    group   => $instance,
    mode    => '0644',
    source  => $source,
    require => Tomcat::Instance[$instance]
  }
}
